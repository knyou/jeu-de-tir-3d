#include "Labyrinthe.h"
#include "Chasseur.h"
#include "Gardien.h"

#include <string>
#include <vector>
#include <iostream>

using namespace std;

Sound*	Chasseur::_hunter_fire;	// bruit de l'arme du chasseur.
Sound*	Chasseur::_hunter_hit;	// cri du chasseur touch�.
Sound*	Chasseur::_wall_hit;	// on a tap� un mur.

extern int longueur_lab(char* const file_name);
extern int largeur_lab (char* const file_name);
extern char** lab_char ( char* filename,int i ,int j);
extern string* lab_affiche (char* filename);

//faire modif pour affiche mur

vector< Wall> renvoie_vec_mur_hori (char** tab, int i, int j) {
	int indice_vector = 0;	
	vector< Wall> stock_mur(0);
	bool extremite_deja_vue = false;
	struct Wall mur;
	for (int k = 0 ; k< i ; k++) {

		for(int l = 0; l < j ; l++) {
		
			if (tab[k][l] == '+')  {
                        	if (!extremite_deja_vue) {					
					mur._x1 = k;
					mur._y1 = l;	extremite_deja_vue = true;
					stock_mur.push_back(mur); 				
				}
				
			}
			if ((tab[k][l] == ' ') && extremite_deja_vue)  {
					mur = stock_mur[indice_vector];
					mur._x2 = k;
					mur._y2 = l-1;
					mur._ntex = 0;
					stock_mur[indice_vector] = mur;
					extremite_deja_vue = false; ++indice_vector;

			}

			if (( l == j-1) && extremite_deja_vue)  {
					mur = stock_mur[indice_vector];
					mur._x2 = k;
					mur._y2 = l;
					mur._ntex = 0;
					stock_mur[indice_vector] = mur;
					extremite_deja_vue = false; ++indice_vector;

			}			

		}

	}
	
	return stock_mur;
} 

//a tester
vector< Wall> renvoie_vec_mur_vert (char** tab, int j, int i) {
	int indice_vector = 0;	
	vector< Wall> stock_mur(0);
	bool extremite_deja_vue = false;
	struct Wall mur;
	for (int k = 0 ; k< j ; k++) {

		for(int l = 0; l < i ; l++) {
		
			if (tab[l][k] == '+')  {
                        	if (!extremite_deja_vue) {					
					mur._x1 = l;
					mur._y1 = k;	extremite_deja_vue = true;
					
					stock_mur.push_back(mur); 				
				}
			}
			if ((tab[l][k] == ' ') && extremite_deja_vue)  {
					mur = stock_mur[indice_vector];
					mur._x2 = l-1;
					mur._y2 = k;
					mur._ntex = 0;
					stock_mur[indice_vector] = mur;
					extremite_deja_vue = false; ++indice_vector;

			}
			if (( l == i-1) && extremite_deja_vue)  {
					mur = stock_mur[indice_vector];
					mur._x2 = l;
					mur._y2 = k;
					mur._ntex = 0;
					stock_mur[indice_vector] = mur;
					extremite_deja_vue = false; ++indice_vector;

			}		


		}

	}
	
	return stock_mur;
} 


char** mur_data_1 (char** data, Wall mur) {

if (mur._y1 == mur._y2) { for(int i = mur._x1; i<= mur._x2; i++ ) { data[i][mur._y2] = 1;} }
else { for(int i = mur._y1; i<= mur._y2; i++ ) { data[mur._x2][i] = 1;}  }


return data;

} 

vector <Box> vec_box (char** tab, int i ,int j) {
vector<Box> vec(0);
Box x;
	for(int k = 0; k < i; k++) {

		for(int l = 0; l < j ; l++)  {

			if (tab[k][l] == 'X'){
			
				x._x = k;
				x._y = l;
				x._ntex = 0;
				vec.push_back(x);
			}
		
				
		}

	}


return vec;

}



vector <Gardien*> vec_gardien (char** tab, int i ,int j, Labyrinthe* lab, int scale, Chasseur* player) {

vector<Gardien*> vec(0);

const char* tab_nom_gardien[4] = {"drfreak","Marvin", "Potator","garde"};
int _compteur=0;
for(int k = 0; k < i; k++) {

	for(int l = 0; l < j ; l++)  {

		if (tab[k][l] == 'G'){
			Gardien* g = new Gardien(lab, tab_nom_gardien[_compteur%4],player, _compteur);
			g->_x = (float)k * (float)scale;
			g->_y = (float)l * (float)scale;
			vec.push_back(g); _compteur++;
		}
				
	}

}

return vec;

}


Chasseur* return_chasseur (char** tab, int i ,int j, Labyrinthe* lab, int scale) {

for(int k = 0; k < i; k++) {

	for(int l = 0; l < j ; l++)  {

		if (tab[k][l] == 'C'){
			
			Chasseur* c = new Chasseur(lab);
			c->_x = (float)k * (float)scale;
			c->_y = (float)l * (float)scale;
			
			return c;
		}
				
	}

}

  throw string("ERREUR: Chasseur pas trouv� dans fichier d'entr� !");

}




Environnement* Environnement::init (char* filename)
{
	return new Labyrinthe (filename);
}

/*
 *	EXEMPLE de labyrinthe.
 */

Labyrinthe::Labyrinthe (char* filename)
{

	vector < Wall >  vec_hor;	
	vector < Wall >  vec_vert;
	lab_height = longueur_lab(filename);
	lab_width = largeur_lab(filename);

	char ** tab = lab_char(filename,largeur_lab(filename),longueur_lab(filename));
	vec_hor = renvoie_vec_mur_hori(tab,largeur_lab(filename),longueur_lab(filename));	
	vec_vert = renvoie_vec_mur_vert(tab,longueur_lab(filename),largeur_lab(filename));
	int taille_tab_mur = vec_hor.size()+vec_vert.size();
	_walls = new Wall [taille_tab_mur];
	vec_hor.insert(vec_hor.end(), vec_vert.begin(), vec_vert.end());
	for (int i = 0; i< (int)vec_hor.size() ; i++ ) { _walls [i] = vec_hor[i]; }
	

	// taille du labyrinthe.
	
	// les murs: 4 dans cet EXEMPLE!

	
	_nwall = taille_tab_mur;
	int i = lab_width; 
	int j = lab_height;
	vector< Wall> stock_mur(0);
	struct Wall mur;
	char caractere;
	string* tab_affiche = lab_affiche(filename);
//on fait 2 parcours du tableau un horizontal pour les affiches horiz et pareils pour vertical
	for (int k = 0 ; k< i ; k++) {

		for(int l = 0; l < j ; l++) {
			
			caractere = tab[k][l];
			if ((int)caractere >=97 && (int)caractere <=122) {
			
				if (l>0 && l<j-1 && (tab[k][l+1]== '-')) // on ne veut pas 
			                                                                      //regarder tab[k][-1] et tab[k][j]
			
				{	mur._x1 = k;                                        //on fait 2 if au cas ou une affiche est coll�e 
					mur._y1 = l;                                        // � une autre ou a un mur
					mur._x2 = k;
					mur._y2 = l+2;
					char	tmp [128];
					string aff = tab_affiche[(int)caractere-97];	
					const char *cstr = aff.c_str();	
					sprintf (tmp, "%s/%s", texture_dir, (char*) cstr);
					mur._ntex = wall_texture (tmp);
					stock_mur.push_back(mur);

				}
				else {
				
					if (l>0 && l<j-1 && (tab[k][l-1]== '-')) // on ne veut pas 
			                                                                      //regarder tab[k][-1] et tab[k][j]
			
				{	mur._x1 = k;
					mur._y1 = l;
					mur._x2 = k;
					mur._y2 = l-2;
					char	tmp [128];
					string aff = tab_affiche[(int)caractere-97];	
					const char *cstr = aff.c_str();	
					sprintf (tmp, "%s/%s", texture_dir, (char*) cstr);
					mur._ntex = wall_texture (tmp);
					stock_mur.push_back(mur);

				}

				}




			}
		}
	}
	
	for (int k = 0 ; k< i ; k++) {

		for(int l = 0; l < j ; l++) {
			
			caractere = tab[k][l];
			if ((int)caractere >=97 && (int)caractere <=122) {
			
				if (k>0 && k<i-1 && (tab[k+1][l]== '|')) // on ne veut pas 
			                                                                      //regarder tab[k][-1] et tab[k][j]
			
				{	mur._x1 = k;
					mur._y1 = l;
					mur._x2 = k+2;
					mur._y2 = l;
					char	tmp [128];
					string aff = tab_affiche[(int)caractere-97];	
					const char *cstr = aff.c_str();	
					sprintf (tmp, "%s/%s", texture_dir, (char*) cstr);
					mur._ntex = wall_texture (tmp);
					stock_mur.push_back(mur);
				}
				else {

					if (k>0 && k<i-1 && (tab[k-1][l]== '|')) //on ne veut pas 
			                                                                      //regarder tab[k][-1] et tab[k][j]
			
				{	mur._x1 = k;
					mur._y1 = l;
					mur._x2 = k-2;
					mur._y2 = l;
					char	tmp [128];
					string aff = tab_affiche[(int)caractere-97];	
					const char *cstr = aff.c_str();	
					sprintf (tmp, "%s/%s", texture_dir, (char*) cstr);
					mur._ntex = wall_texture (tmp);
					stock_mur.push_back(mur);
				}
				}



			}
		}
	}
	_picts = new Wall [stock_mur.size()];
	for(int i = 0; i<(int)stock_mur.size(); i++ ) {
	_picts[i] = stock_mur[i];
	}
	

	_npicts = stock_mur.size();
	
	vector<Box> vector_box = vec_box(tab, lab_width, lab_height);
	_boxes = new Box [vector_box.size()];
	for(int i = 0; i< (int)vector_box.size() ; i++ ) {
	_boxes[i] = vector_box[i];
	}
 	_nboxes = vector_box.size() ;

// cr�ation du tableau d'occupation du sol.
	_data = new char* [lab_width];
	for (int i = 0; i < lab_width; ++i)
		_data [i] = new char [lab_height];
	// initialisation du tableau d'occupation du sol.
	for (int i = 0; i < lab_width; ++i)
		for (int j = 0; j < lab_height; ++j) {
			_data [i][j] = EMPTY;
		}
	for (int i = 0; i< (int)vec_hor.size(); i++) {
		_data = mur_data_1 (_data, vec_hor[i]);
	
	}
	
	for(int i = 0; i<(int)vector_box.size() ; i++) {

		_data [_boxes [i]._x][_boxes [i]._y] = 1;

	}
		
	for(int i=0 ; i< lab_width ; i++) {
		for (int j=0 ; j< lab_height ; j++) {
			if (tab[i][j] == 'T') {
				_treasor._x = i;
				_treasor._y = j;
				_data [_treasor._x][_treasor._y] = 1;	
			}
		}

	}

	// le chasseur et les gardiens.
	Chasseur* joueur = return_chasseur(tab, lab_width, lab_height, this, scale);
	vector <Gardien*> vector_de_gardien = vec_gardien (tab, lab_width, lab_height, this, scale, joueur);
	_nguards =vector_de_gardien.size() + 1;
	_guards = new Mover* [_nguards];
	_guards[0] = joueur;
	for (int i = 1; i< _nguards; i++) {
	
		_guards[i] = vector_de_gardien[i-1];
		
		
	}
	//_guards[1]->_angle  = 180;
	//_guards[3]->_angle  = 180;
	for(int i =1; i<12; i++){char** copie = ((Gardien*)_guards[i])->copie_data ();
            cout << ((Gardien*)_guards[i])->distance_gardien_tresor (copie)<< endl;                                                                         
	//cout << (((Gardien*)_guards[i])->analyse_champs_de_vision (copie)).trouve<< endl;
}
	/*_nguards = 2;
	_guards [0] = new Chasseur (this);_guards [0] -> _x = 10.;_guards [0] -> _y = 10.;_guards [1] = new Gardien (this, "drfreak");
	
_guards [1] -> _x = 20.; _guards [1] -> _y = 20.;//_data [(int)(_guards [1] -> _x / scale)][(int)(_guards [1] -> _y / scale)] = 1;*/

/*
	_guards [2] = new Gardien (this, "Marvin"); 
	_guards [3] = new Gardien (this, "Potator"); _guards [3] -> _x = 60.; _guards [3] -> _y = 10.;
	_guards [4] = new Gardien (this, "garde"); _guards [4] -> _x = 130.; _guards [4] -> _y = 100.;

	// indiquer qu'on ne marche pas sur les gardiens.
	
	_data [(int)(_guards [2] -> _x / scale)][(int)(_guards [2] -> _y / scale)] = 1;
	_data [(int)(_guards [3] -> _x / scale)][(int)(_guards [3] -> _y / scale)] = 1;
	_data [(int)(_guards [4] -> _x / scale)][(int)(_guards [4] -> _y / scale)] = 1;*/
}
