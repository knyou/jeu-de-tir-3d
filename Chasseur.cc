#include "Chasseur.h"
#include "Gardien.h"
#include <cmath>
/*
 *	Tente un deplacement.
 */

bool Chasseur::move_aux (double dx, double dy)
{
	if (EMPTY == _l -> data ((int)((_x + dx) / Environnement::scale),
							 (int)((_y + dy) / Environnement::scale)))
	{
		_x += dx;
		_y += dy;
		return true;
	}
	return false;
}

/*
 *	Constructeur.
 */

Chasseur::Chasseur (Labyrinthe* l) : Mover (100, 80, l, 0)
{
	_hunter_fire = new Sound ("sons/hunter_fire.wav");
	_hunter_hit = new Sound ("sons/hunter_hit.wav");
	if (_wall_hit == 0)
		_wall_hit = new Sound ("sons/hit_wall.wav");
}

/*
 *	Fait bouger la boule de feu (ceci est une exemple, � vous de traiter les collisions sp�cifiques...)
 */



int Chasseur::gardien_touche(float x, float y) {

	float	dmax2 = (_l -> width ())*(_l -> width ()) + (_l -> height ())*(_l -> height ());

	for (int i = 1; i < _l->_nguards; i++) {

		float distance2 = (x - (_l->_guards[i]->_x)/(float)Environnement::scale) * 
				 (x - (_l->_guards[i]->_x)/(float)Environnement::scale) +
				 (y - (_l->_guards[i]->_y)/(float)Environnement::scale) * 
				 (y - (_l->_guards[i]->_y)/(float)Environnement::scale);
		float distance = sqrt(distance2);
		if (distance <= 1.) 
		{	int pt_life = ((Gardien *)(_l->_guards[i]))->get_point_de_vie();
			((Gardien *)(_l->_guards[i]))->set_vue();
			if (pt_life > 1) { _l->_guards[i]->tomber (); 
					   ((Gardien *)((_l->_guards[i])))->set_point_de_vie(pt_life - 1);
					   message("Gardien touch�");
					   ((Gardien *)((_l->_guards[i])))-> _guard_hit -> play (1 - distance2/dmax2);
					 }
			else {
			message("Gardien mort");
			((Gardien *)(_l->_guards[i]))->is_dead();
			_l->_guards[i]->rester_au_sol ();
			((Gardien *)((_l->_guards[i])))-> _guard_die -> play (1 - distance2/dmax2);
			}
			return i;
		}
	}	
	return -1;

}

int Chasseur::box_touche(int x, int y) {

	for(int i = 0; i < (_l->_nboxes); i++) {
		
		if (x == _l->_boxes[i]._x && y == _l->_boxes [i]._y)
	        {
		point_de_vie += 2;
		message("bonus point de vie");
		((Labyrinthe*)_l)->set_data(x,y,EMPTY);
		return i;
		}
		
	}
	return -1;
}


void Chasseur::reinitialise_lab_avec_une_boxe_en_moins(int num) {
	int nv_taille = (_l->_nboxes) - 1;
	Box* nv_tab = new Box [nv_taille];
	bool box_trouve = false;
	for (int i = 0; i < nv_taille + 1; i++) {

		if ( i != num) {
			if(!box_trouve){nv_tab[i] = (_l->_boxes[i]);}
			else {nv_tab[i-1] = (_l->_boxes[i]);}
		}
		else { box_trouve = true; }

	}
	_l->_boxes = new Box [nv_taille];
	for (int i = 0; i < nv_taille; i++) {

		_l->_boxes[i] = nv_tab[i];
	}
	_l->_nboxes = nv_taille;
	_l->reconfigure();
}


bool Chasseur::process_fireball (float dx, float dy)
{
	
 // calculer la distance entre le chasseur et le lieu de l'explosion.
	float	x = (_x - _fb -> get_x ()) / Environnement::scale;
	float	y = (_y - _fb -> get_y ()) / Environnement::scale;
	float	dist2 = x*x + y*y;
	// on bouge que dans le vide!
	if (EMPTY == _l -> data ((int)((_fb -> get_x () + dx) / Environnement::scale),
							 (int)((_fb -> get_y () + dy) / Environnement::scale)))
	{
		//message ("Woooshh ..... %d", (int) dist2);
		// il y a la place.
		return true;
	}
	else {	int a = box_touche(((_fb -> get_x () + dx) / (float)Environnement::scale),
				     ((_fb -> get_y () + dy) / (float)Environnement::scale));
		if (a != -1){reinitialise_lab_avec_une_boxe_en_moins(a);}
	
		if (gardien_touche(((_fb -> get_x () + dx) / (float)Environnement::scale),
				     ((_fb -> get_y () + dy) / (float)Environnement::scale)) != -1) {}
		
	}
	// collision...
	// calculer la distance maximum en ligne droite.
	float	dmax2 = (_l -> width ())*(_l -> width ()) + (_l -> height ())*(_l -> height ());
	// faire exploser la boule de feu avec un bruit fonction de la distance.
	_wall_hit -> play (1. - dist2/dmax2);
	//message ("Booom...");
	// teste si on a touch� le tr�sor: juste pour montrer un exemple de la
	// fonction � partie_terminee �.
	if ((int)((_fb -> get_x () + dx) / Environnement::scale) == _l -> _treasor._x &&
		(int)((_fb -> get_y () + dy) / Environnement::scale) == _l -> _treasor._y)
	{
		partie_terminee (true);
	}
	return false;

}

/*
 *	Tire sur un ennemi.
 */

void Chasseur::fire (int angle_vertical)
{
	//message ("Woooshh...");
	_hunter_fire -> play ();
	_fb -> init (/* position initiale de la boule */ _x, _y, 10.,
				 /* angles de vis�e */ angle_vertical, _angle);
}

/*
 *	Clic droit: par d�faut fait tomber le premier gardien.
 *
 *	Inutile dans le vrai jeu, mais c'est juste pour montrer
 *	une utilisation des fonctions � tomber � et � rester_au_sol �
 */

void Chasseur::right_click (bool shift, bool control) {
	if (shift)
		_l -> _guards [1] -> rester_au_sol ();
	else
		_l -> _guards [1] -> tomber ();
}

int Chasseur::get_point_de_vie() {return point_de_vie;};
void Chasseur::set_point_de_vie(int pt) { point_de_vie = pt; };

