#ifndef GARDIEN_H
#define GARDIEN_H


#include "Mover.h"
#include "Chasseur.h"
#include "Labyrinthe.h"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

class Labyrinthe;

enum Etat {Patrouilleur, Defenseur, Attaquant};

class Gardien : public Mover {
private:
	int dead = false;
	int point_de_vie = 3;
	int clock;
	bool tir_en_cours = false;
	bool chasseur_just_seen = false;
	int compteur = 0;
	int compteur_fire = 0;
	int x_chasseur;
	int y_chasseur;
	int longueur_vision;
	float speed_x = 0.05;
	float speed_y = 0.05;
	float rad = 0.;
	float distance_garde_tresor = 15.;
	int coord_tresor_x;
	int coord_tresor_y;
	Chasseur* chasseur;
	float seuil_treasor = 1/16.;
	Etat etat;
	int id;
	float radian_permanent;
	
public: 
	Sound*	_guard_fire;	// bruit de l'arme du gardien
	Sound*	_guard_hit;	// cri du gardien touché.
        Sound*	_guard_die;	// cri gardien mort	

	Gardien (Labyrinthe* l, const char* modele, Chasseur* chass, int identif);
	void update (void);

	bool move (double dx, double dy) ;

	// et ne bouge pas!
	bool move_aux (double dx, double dy) ;
	
	
	
        void fire (int angle_vertical);
	bool process_fireball (float dx, float dy);
	
	struct Case {
	int x, y, compteur;
	};

	int** copie_lab_0_1 (char** lab, int i, int j);

	struct Info_Chasseur{
	
	bool trouve;
	float angle;

	};


// mur == 1; espace vide deja visité == 1; sinon 0 (tt ça dans tab_initialise_avec_0_1)
	int distance_gardien_tresor(char** copie_0_1);

	Info_Chasseur analyse_champs_de_vision (char** copie_0_1);
	
	void aller_au_point_x_y(int x, int y);

	bool move_autour_tresor(double dx, double dy);

	bool move_chasseur(double dx, double dy);
	
	char** copie_data();
	
	int get_point_de_vie();
	
	void set_point_de_vie(int pt);

	void set_vue();

	void is_dead ();

	bool get_dead();
};
#endif
