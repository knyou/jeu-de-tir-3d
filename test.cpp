#include <iostream>
#include <string>
#include <fstream>
#include "Environnement.h" 
#include "Labyrinthe.h"
#include <vector>
using namespace std;
 
//faire un generateur alea de lab 

// fonctions qui donnent la long et la larg du lab a partir du fichier txt.
int longueur_lab (char* const file_name) {
	int compteur_sauv = 0;
	int compteur_cour = 0;
	ifstream fichier(file_name, ios::in);
        if(fichier)  
        {	char caractere;
		   
		while(fichier) {
		fichier.get(caractere);  
		if (caractere == '+') { compteur_cour = 1;
                                        break; }  
		}
     	
		while(fichier){
                fichier.get(caractere);  
		if (caractere == '\n') { if (compteur_cour> compteur_sauv)  { compteur_sauv = compteur_cour;  } 
                                         compteur_cour = 0;}  
		else {compteur_cour++;}              
                }fichier.close();
        }
        else
                cerr << "Impossible d'ouvrir le fichier !" << endl;
 

	return compteur_sauv;

} 


int largeur_lab (char* const filename) {

	int compteur = 0;
        ifstream fichier(filename, ios::in);  
        if(fichier)  
        {	
		char caractere;
		while(fichier) {
		fichier.get(caractere);  
		if (caractere == '+') { string contenu;  
                			getline(fichier, contenu); 
					++compteur; 
                                        break; }  
		}
		while(fichier) {
                string contenu;  
                getline(fichier, contenu); 
                ++compteur; 
 		}
                fichier.close(); 
        }
        else
                cerr << "Impossible d'ouvrir le fichier !" << endl;
        
	return compteur-1;

}

int compte_nb_ligne_avant_lab (char* filename) {

int compteur = 0;
char caractere;
ifstream fichier(filename, ios::in);

while(fichier) {
		fichier.get(caractere);  
		if (caractere == '\n') { ++compteur;  }
		if (caractere == '+') { break; }  
		} fichier.close();
return compteur;
}

// fonction qui stocke dans un tab de char le labyrinthe a partir du fichier txt.

char** lab_char ( char* filename,int i ,int j) {
	int compteur;	
	int compteur_i = 0;
	int compteur_j = 0;
	char** stock_filename = new char*[i];
	for (int k = 0; k < i; k++) {stock_filename[k] = new char[j];}
        for (int k = 0; k<i ; k++ ) {
		for (int l = 0; l < j ; l++ ) { stock_filename[k][l] = ' ' ; } }
	ifstream fichier(filename, ios::in);
        if(fichier)  
        {	char caractere;
		   
		compteur = compte_nb_ligne_avant_lab(filename);
     		ifstream fichier(filename, ios::in);
		string contenu;
		for (int p= 0; p<compteur ; p++) { getline(fichier, contenu); }		
		while(fichier){
                fichier.get(caractere);  
		if (caractere == '\n') { ++compteur_i;
 					 compteur_j = 0;}  
		else { stock_filename[compteur_i][compteur_j] = caractere ; ++compteur_j ;}              
                }fichier.close();
        }
        else
                cerr << "Impossible d'ouvrir le fichier !" << endl;
 

	return stock_filename;

}



bool test_deb_lab (string word) {

		for (int i = 0; i<(int)word.size() ; i++) {
			if (word[i] == ' ' ) {}
			else { if (word[i] == '+')  {return true;} }
		}

		return false;
}


//met dans un tableau de string le .jpg de texture a la place qui va bien

string* lab_affiche (char* filename) {

	int indice;
	ifstream file(filename); 
        string word; 
        string* tab_affiche = new string [26]; 
	for (int i = 0 ; i< 26 ; i++) {
		tab_affiche[i] = "";
	}
        while( file >> word ) 

	{	char caractere;
       		if (word == "#") {  while( file.get(caractere) )  { if (caractere == '\n')   { break; }}    ;}
		else {
			const char *cstr = word.c_str();
			char b = (char) *cstr;	
			if ((int)b >=97 && (int)b <=122)  {
					indice = (int) b -97;
					file >> word; tab_affiche[indice] = word ; 
					} 
		       else { if (test_deb_lab(word)) {break;} }

		}

 
}

return tab_affiche;

}

/*
vector<const char*> lab_gardien (char* filename) {

	int indice;
	ifstream file(filename); 
        string word; 
        vector<const char*> tab_gardien = new vector<const char*>(0); 
	
        while( file >> word ) 
	{
       		if (word == "Gardien:") {  break;}
		
		}
	else {
			const char *cstr = word.c_str();
			char b = (char) *cstr;	
			if ((int)b >=97 && (int)b <=122)  {
					indice = (int) b -97;
					file >> word; tab_affiche[indice] = word ; 
					} 
		       else { if (test_deb_lab(word)) {break;} }

 
}

return tab_affiche;

}
*/



void affiche_wall(Wall mur) {

cout << "depart :  " << mur._x1 << "   " << mur._y1<< endl;
cout << "arrive :  " << mur._x2 << "   " << mur._y2<< endl;


}


/*
{
        ifstream fichier("text.sim", ios::in);  // on ouvre en lecture
 
        if(fichier)  // si l'ouverture a fonctionné
        {	while(fichier){
                
		char caractere;  // notre variable où sera stocké le caractère
                fichier.get(caractere);  // on lit un caractère et on le stocke dans caractere
                cout << caractere;  // on l'affiche

                }fichier.close();
        }
        else
                cerr << "Impossible d'ouvrir le fichier !" << endl;
 	string file_s = "text.sim";
	const char* file_c = file_s.c_str();
	cout <<"long  " <<longueur_lab((char*)file_c)+2 <<endl;
	cout <<"larg  " <<largeur_lab((char*)file_c) +2<<endl;


	char** tab = lab_char((char*)file_c,largeur_lab((char*)file_c)+2,longueur_lab((char*)file_c)+2);
	
	for (int p = 0; p< largeur_lab((char*)file_c)+2 ; p++) {
		for (int m=0 ; m< longueur_lab((char*)file_c)+2; m++) {cout << tab[p][m];}
			cout << endl;
		}


	
	
	vector < Wall >  vec_hor;

	


	
	/*vector < Wall >  vec_vert;
	int lab_height = longueur_lab((char*)file_c);
	int lab_width = largeur_lab((char*)file_c);
	vec_hor = renvoie_vec_mur_hori(tab,lab_width+2,lab_height+2);	
	vec_vert = renvoie_vec_mur_vert(tab,lab_height+2,lab_width+2);
	vec_hor.insert(vec_hor.end(), vec_vert.begin(), vec_vert.end());
	for(int i= 0; i< vec_hor.size(); i++)  {
	cout << "mur  " <<i  << endl;  affiche_wall(vec_hor[i]);


	}*/	
	//string* aff = lab_affiche((char*)file_c);
	//int compteur = 0; 
	//for (int i = 0 ; i< 26 ; i++) { if (aff[i] == "") { ++compteur;}    }
  //      return 0;
//} */ */

