#include "Gardien.h"

//mettre la vitesse de déplacement en fonction de l'état: attaque vitesse max, def min, alea moy.
//éviter que les gardiens se bloquent.
//chaque minute, faire pop un gardien (si max_gard > gard)
//changer les attributs du gardien (directement accessible via attribut environnement)
//faire les sons
//a faire : tout reconfig dans chasseur.cc
//faire un test de cout: 0 si voit pas le chasseur, 1 sinon
//lorsqu'on tue un gardien mettre dans data 0
//rajouter un attibut mort
float somme_potentiel = 0.;


Gardien::Gardien (Labyrinthe* l, const char* modele, Chasseur* chass, int identif) : Mover (120, 80, l, modele)
	{longueur_vision = 25;
	 chasseur = chass;
	 coord_tresor_x = _l->_treasor._x;
	 coord_tresor_y = _l->_treasor._y;
	 id = identif;
	 etat = Patrouilleur;
	_guard_fire = new Sound ("sons/guard_fire.wav");
	_guard_hit = new Sound ("sons/guard_hit.wav");
	_guard_die = new Sound ("sons/guard_die.wav");
	}


char** Gardien::copie_data() {

	int w = ((Labyrinthe*)(this->_l))->width();
	int h = ((Labyrinthe*)(this->_l))->height();
	char** test = new char*[w];
	for (int k = 0; k < w; k++) {test[k] = new char[h];}

	for(int k = 0; k< w; k++) {
		for(int l = 0; l <h ; l++) {
			test[k][l] = ((Labyrinthe*)(this->_l))->data(k,l);
		}

 
	}
	
	return test;

}


//le tir d'un gardien doit avoir une certaine imprécision
// genere une imprecision entre -0.1  et 0.1 radian
float genere_imprecision_alea () {

	 return ( (float)rand()/(float)RAND_MAX ) * 0.2 - 0.2/2.;

}


void Gardien::update (void) {


		if(etat == Patrouilleur) {speed_x = 0.05;
	 				  speed_y = 0.05;}
		if(etat == Defenseur) {speed_x = 0.04;
	 				  speed_y = 0.04;}
		if(etat == Attaquant) {speed_x = 0.06;
	 			       speed_y = 0.06;}
		
 		if (!dead) {
		char** copie = this->copie_data();

		char** copie_2 = this->copie_data();
		Info_Chasseur inf_ch = analyse_champs_de_vision(copie);
		float distance_tresor;
		if (id == 0) { somme_potentiel = 0.; } // le premier gardien initialise le potentiel
		//faire le potentiel que si gardien vivant
	        if(somme_potentiel < seuil_treasor && !dead ){  
				//on requisitionne des patrouilleur si condition verifié.
				if(etat == Patrouilleur) {

					etat = Defenseur;
					_x = coord_tresor_x * Environnement::scale;
					_y = coord_tresor_y * Environnement::scale;
				}
				
				distance_tresor = (float)distance_gardien_tresor(copie_2);
				somme_potentiel += 1./distance_tresor ; 
		}
				
			//libérez les défenseur quand le trésor est gardé.
		else {
			
			if(etat == Defenseur) { etat = Patrouilleur; }

		}

		// partie tir
		if (tir_en_cours) { // si un tir est en cours, on ne tire pas à nouveau
			
			if (process_fireball(-sin(radian_permanent)*10,cos(radian_permanent)*10)){ }
			else {tir_en_cours = false;}
		
		}
	 	compteur_fire++;
		if (inf_ch.trouve) { 
		etat = Attaquant;
		if (inf_ch.angle < 0) { inf_ch.angle = 2.*M_PI + inf_ch.angle;}
		//cout << this->_angle << endl;
		this->_angle = (int) ((inf_ch.angle/M_PI)*180.);
		
		this->rad = inf_ch.angle; 
		if (tir_en_cours == false && compteur_fire % 40 == 0) // si tire pas en cours et interval d'attente passé, on initialise un tir, le tir a un angle fixe.
			{fire(0); radian_permanent = fmod(this->rad /*+ genere_imprecision_alea()*/,2.*M_PI ); 
			tir_en_cours = true;
 			compteur_fire = 0;
			_guard_fire->play();}
		
		//cout << (int) ((inf_ch.angle/M_PI)*180.) << endl;		
		//cout << _angle << endl;
		move_chasseur(speed_x*Environnement::scale*(-sin(rad)),speed_y*Environnement::scale*cos(rad));
		}
		else{ if(etat == Attaquant) {
			//ce bout de code est fait pour qu'un gardien ayant juste perdu de vu le chasseur ne passe pas en mode 
			//patrouilleur avant d'avoir atteint le dernier  endroit ou il l'a vu
			//faire avec la distance  au lieu des coord.

				if(!move(speed_x*Environnement::scale*(-sin(rad)),speed_y*Environnement::scale*cos(rad))) {}
		
			/*else{
				if(compteur < 4) { _angle = (_angle + 90)%360; //fait un tour sur lui-meme qd le chasseur disparait. 
						   rad = (M_PI*(float)_angle)/180.; //cout << "turn" << endl;
						   compteur++; }
				else { compteur = 0; chasseur_just_seen = false; }
			    }*/
	
			etat = Patrouilleur;
			
	
			
			}		

		}
		if (etat == Patrouilleur) {
		if(!move(speed_x*Environnement::scale*(-sin(rad)),speed_y*Environnement::scale*cos(rad))) {
			_angle=(rand()%360);
                    	rad=(M_PI*(float)_angle)/180.;}
		}
		if (etat == Defenseur) {

			if(!move_autour_tresor(speed_x*Environnement::scale*(-sin(rad)),speed_y*Environnement::scale*cos(rad))) {
			_angle=(rand()%360);
                        rad=(M_PI*(float)_angle)/180.;}

		}} 
	
	};
//marche aléatoire pour le patrouilleur
bool Gardien::move (double dx, double dy) { 
		return move_aux (dx, dy);
	}

	// et ne bouge pas!
bool Gardien::move_aux (double dx, double dy) { 
		bool vide = (EMPTY == _l -> data ((int)((_x + dx) / Environnement::scale),
							 (int)((_y + dy) / Environnement::scale)));
		

		if (vide || (!vide && ((int)_x / Environnement::scale ==(int)(_x+dx)/ Environnement::scale) && 
                            ((int)_y / Environnement::scale ==(int)(_y+dy)/ Environnement::scale)))
		{
			this->_l->set_data((int)_x/Environnement::scale,(int)_y/Environnement::scale,0);
			_x += dx;
			_y += dy;
			this->_l->set_data((int)_x/Environnement::scale,(int)_y/Environnement::scale,1);
			return true;
		}
		return false;
}
	
//marche autour du trésor
bool Gardien::move_autour_tresor(double dx, double dy) {

	bool vide = (EMPTY == _l -> data ((int)((_x + dx) / Environnement::scale),
							 (int)((_y + dy) / Environnement::scale)));
		
	float distance_vol_doiseau_tresor_2 = ((((this->_x + dx)/Environnement::scale) - this->coord_tresor_x)*
					      (((this->_x + dx)/Environnement::scale) - this->coord_tresor_x)) + 
                                              ((((this->_y + dy)/Environnement::scale) - this->coord_tresor_y)*
					      (((this->_y + dy)/Environnement::scale) - this->coord_tresor_y));
	float distance_vol_doiseau_tresor = sqrt(distance_vol_doiseau_tresor_2);
	if ((vide || (!vide && ((int)_x / Environnement::scale==(int)(_x+dx)/ Environnement::scale) && 
                     ((int)_y / Environnement::scale==(int)(_y+dy)/ Environnement::scale)))
             && distance_vol_doiseau_tresor <= this->distance_garde_tresor)
		{
			this->_l->set_data((int)_x/Environnement::scale,(int)_y/Environnement::scale,0);
			_x += dx;
			_y += dy;
			this->_l->set_data((int)_x/Environnement::scale,(int)_y/Environnement::scale,1);
			return true;
		}
		return false;

}

//cours apres le chasseur et s'arrete à 2.5 cases de lui
bool Gardien::move_chasseur(double dx, double dy/*, Gardien::Info_Chasseur inf_ch*/){

	//this->_angle = (int) ((inf_ch.angle/M_PI)*180.);
	//this->rad = inf_ch.angle;
	
	
	bool vide = (EMPTY == _l -> data ((int)((_x + dx) / Environnement::scale),
							 (int)((_y + dy) / Environnement::scale)));

	float distance_garde_chasse = ((float)this->x_chasseur - this->_x / (float)Environnement::scale)*
				      ((float)this->x_chasseur - this->_x / (float)Environnement::scale) +
				      ((float)this->y_chasseur - this->_y / (float)Environnement::scale)*
				      ((float)this->y_chasseur - this->_y / (float)Environnement::scale); 
	distance_garde_chasse = sqrt(distance_garde_chasse);
	if (  (vide || (!vide && ((int)_x /Environnement::scale==(int)(_x+dx)/ Environnement::scale) && 
            ((int)_y / Environnement::scale==(int)(_y+dy)/ Environnement::scale))) && distance_garde_chasse > 2.5 )
		{ //faire avec les distances plutot.
			this->_l->set_data((int)_x/Environnement::scale,(int)_y/Environnement::scale,0);
			_x += dx;
			_y += dy;
			this->_l->set_data((int)_x/Environnement::scale,(int)_y/Environnement::scale,1);
			return true;
		}
	if (!vide) { etat = Patrouilleur;}
	return false;

}




int** Gardien::copie_lab_0_1 (char** lab, int i, int j){
		int** copie = new int*[i];
	for (int k = 0; k < i; k++) {copie[k] = new int[j];}

		for(int k = 0; k< i; k++) {
			for(int l = 0; l <j ; l++) {
//attention, ici changer 'a' 'b' par toute les lettres de l'alphabet.
				if (lab[k][l] == '|' || lab[k][l] == '-' || ((int)lab[k][l] >= 97 && (int)lab[k][l] <= 122)|| 
                                    lab[k][l] == '+' || lab[k][l] == 'X') { copie[k][l] = 1;} 
				else {copie[k][l] = 0;}
			}


		}


	return copie;



	}



// parcours en largeur de _data qui donne la distance réel en un gardien et le trésor.
	int Gardien::distance_gardien_tresor(char** copie_0_1){
			
		Case gardien;
		gardien.x = (this->_x)/this->_l->scale;
		gardien.y = (this->_y)/this->_l->scale;
		gardien.compteur = 0;
		vector<Case> vec_case(0);		
		vec_case.push_back(gardien);
		copie_0_1[gardien.x][gardien.y] = 1;
		//a cause de la teleportation des gardiens
		if(gardien.x == coord_tresor_x && gardien.y == coord_tresor_y) {return 1;}

		while(vec_case.size() != 0) {
			
			Case en_cours = vec_case[0];
			vec_case.erase(vec_case.begin());
			if(copie_0_1[en_cours.x][en_cours.y+1] == EMPTY || (coord_tresor_x == en_cours.x && 
									    coord_tresor_y == en_cours.y+1 )) {
				if((coord_tresor_x == en_cours.x && coord_tresor_y == en_cours.y+1 )) {return en_cours.compteur + 1;}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x;
				nv_case.y = en_cours.y + 1;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x][en_cours.y-1] == EMPTY || (coord_tresor_x == en_cours.x && 
									    coord_tresor_y == en_cours.y-1 )) {
				if((coord_tresor_x == en_cours.x && coord_tresor_y == en_cours.y-1 )) {return en_cours.compteur + 1;}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x;
				nv_case.y = en_cours.y - 1;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x+1][en_cours.y+1] == EMPTY || (coord_tresor_x == en_cours.x + 1 && 
									    coord_tresor_y == en_cours.y+1 )) {
				if((coord_tresor_x == en_cours.x + 1 && coord_tresor_y == en_cours.y+1 )) {return en_cours.compteur + 1;}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x + 1;
				nv_case.y = en_cours.y + 1;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x - 1][en_cours.y+1] == EMPTY || (coord_tresor_x == en_cours.x -1 && 
									    coord_tresor_y == en_cours.y+1 )) {
				if((coord_tresor_x == en_cours.x - 1 && coord_tresor_y == en_cours.y+1 )) {return en_cours.compteur + 1;}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x - 1;
				nv_case.y = en_cours.y + 1;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x + 1][en_cours.y-1] == EMPTY || (coord_tresor_x == en_cours.x+1 && 
									    coord_tresor_y == en_cours.y-1 )) {
				if((coord_tresor_x == en_cours.x+1 && coord_tresor_y == en_cours.y-1 )) {return en_cours.compteur + 1;}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x + 1;
				nv_case.y = en_cours.y - 1;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x-1][en_cours.y-1] == EMPTY || (coord_tresor_x == en_cours.x-1 && 
									    coord_tresor_y == en_cours.y-1 )) {
				if((coord_tresor_x == en_cours.x-1 && coord_tresor_y == en_cours.y-1 )) {return en_cours.compteur + 1;}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x-1;
				nv_case.y = en_cours.y - 1;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x - 1][en_cours.y] == EMPTY || (coord_tresor_x == en_cours.x-1 && 
									    coord_tresor_y == en_cours.y )) {
				if((coord_tresor_x == en_cours.x-1 && coord_tresor_y == en_cours.y )) {return en_cours.compteur + 1;}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x - 1;
				nv_case.y = en_cours.y;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x + 1][en_cours.y] == EMPTY || (coord_tresor_x == en_cours.x +1 && 
									    coord_tresor_y == en_cours.y )) {
				if((coord_tresor_x == en_cours.x  + 1 && coord_tresor_y == en_cours.y )) {return en_cours.compteur + 1;}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x + 1;
				nv_case.y = en_cours.y;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}

		}

	return -1;




	}

//attention son champs de vision est de 180 degre
//faire comme si il voyait à travers les mur et comme si il avait un vision à 360 deg.
//une fois qu'on a trouvé le chasseur vérifier que ça vérifie ma condition du dessus
//si oui alors c'est bon sinon continuer l'expension du cercle.
//pour la premiere on utilise les 2 coord pour tracer une droite entre gardien et chasseur 

//méthode pour que le gardien aille au trésor: dans la structure rajouter un champs: parent. Lors de l'exploration mettre le parent dans la case, quand on va retourner la case on va pouvoir remonter le file.
      Gardien::Info_Chasseur Gardien::analyse_champs_de_vision (char** copie_0_1) {

		Info_Chasseur inf_ch;
		inf_ch.trouve = false;

		bool chasseur_trouve = false;
		int chasseur_x = (int)this->chasseur->_x/chasseur->_l->scale;
		int chasseur_y = (int)this->chasseur->_y/chasseur->_l->scale;
		Case gardien;
		int compteur_distance = this->longueur_vision;
		gardien.x = (this->_x)/this->_l->scale;
		gardien.y = (this->_y)/this->_l->scale;
		gardien.compteur = 0;
		vector<Case> vec_case(0);		
		vec_case.push_back(gardien);
		copie_0_1[gardien.x][gardien.y] = 1;
		while(vec_case.size() != 0 && compteur_distance>-1) {
			
			Case en_cours = vec_case[0];
			vec_case.erase(vec_case.begin());
			if(copie_0_1[en_cours.x][en_cours.y+1] == 0) {
				if(chasseur_x == en_cours.x && chasseur_y == en_cours.y+1 && en_cours.compteur <= compteur_distance) 
                                {
                                	chasseur_trouve = true;
				}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x;
				nv_case.y = en_cours.y + 1;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x][en_cours.y-1] == 0) {
				if(chasseur_x == en_cours.x && chasseur_y == en_cours.y-1 && en_cours.compteur <= compteur_distance) 
                                {
                                	chasseur_trouve = true;
				}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x;
				nv_case.y = en_cours.y - 1;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x+1][en_cours.y+1] == 0) {
				if(chasseur_x == en_cours.x+1 && chasseur_y == en_cours.y+1 && en_cours.compteur <= compteur_distance) 
                                {
                                	chasseur_trouve = true;
				}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x + 1;
				nv_case.y = en_cours.y + 1;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x - 1][en_cours.y+1] == 0) {
				if(chasseur_x == en_cours.x-1 && chasseur_y == en_cours.y+1 && en_cours.compteur <= compteur_distance) 
                                {
                                	chasseur_trouve = true;
				}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x - 1;
				nv_case.y = en_cours.y + 1;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x + 1][en_cours.y-1] == 0) {
				if(chasseur_x == en_cours.x + 1 && chasseur_y == en_cours.y-1 && en_cours.compteur <= compteur_distance) 
                                {
                                	chasseur_trouve = true;
				}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x + 1;
				nv_case.y = en_cours.y - 1;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x-1][en_cours.y-1] == 0) {
				if(chasseur_x == en_cours.x-1 && chasseur_y == en_cours.y-1 && en_cours.compteur <= compteur_distance) 
                                {
                                	chasseur_trouve = true;
				}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x-1;
				nv_case.y = en_cours.y - 1;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x - 1][en_cours.y] == 0) {
				if(chasseur_x == en_cours.x-1 && chasseur_y == en_cours.y && en_cours.compteur <= compteur_distance) 
                                {
                                	chasseur_trouve = true;
				}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x - 1;
				nv_case.y = en_cours.y;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}
			if(copie_0_1[en_cours.x + 1][en_cours.y] == 0) {
				if(chasseur_x == en_cours.x + 1 && chasseur_y == en_cours.y && en_cours.compteur <= compteur_distance ) 
                                {
                                	chasseur_trouve = true;
				}
				Case nv_case;
				nv_case.compteur = en_cours.compteur + 1;
				nv_case.x = en_cours.x + 1;
				nv_case.y = en_cours.y;
				copie_0_1[nv_case.x][nv_case.y] = 1;
				vec_case.push_back(nv_case);
			}

		}
 

		if(chasseur_trouve) {
//on calcul l'angle entre le chasseur le gardien et un point droit devant le chasseurr pour déterminé si le chasseur est dan sle champs de vision du gardien (on utilise une formule de trigo).
			
			bool est_dans_champs_de_vision = false;			
			float radian=(M_PI*(float)_angle)/180.;
			float nv_point_x = -sin(radian) + (this->_x)/(float)this->_l->scale;
			float nv_point_y = cos(radian)  + (this->_y)/(float)this->_l->scale;
			float distance_2_g_nv = ((this->_x)/(float)this->_l->scale - nv_point_x)*
                                                ((this->_x)/(float)this->_l->scale - nv_point_x)+
                                                ((this->_y)/(float)this->_l->scale - nv_point_y)*
						((this->_y)/(float)this->_l->scale - nv_point_y);
                        float distance_g_nv = sqrt(distance_2_g_nv);
			float chasseur_x_float = this->chasseur->_x/(float)chasseur->_l->scale;
			float chasseur_y_float = this->chasseur->_y/(float)chasseur->_l->scale;
			float distance_2_g_ch = ((this->_x)/(float)this->_l->scale - chasseur_x_float)*
						((this->_x)/(float)this->_l->scale - chasseur_x_float)+
                                                ((this->_y)/(float)this->_l->scale - chasseur_y_float)*
						((this->_y)/(float)this->_l->scale - chasseur_y_float);
                        float distance_g_ch = sqrt(distance_2_g_ch);

			float angle_3_points = acos(( ((this->_x)/(float)this->_l->scale - nv_point_x)*
						      ((this->_x)/(float)this->_l->scale - chasseur_x_float) + 
                                                      ((this->_y)/(float)this->_l->scale - nv_point_y)*
						    ((this->_y)/(float)this->_l->scale - chasseur_y_float))/ (distance_g_ch*distance_g_nv));
			
			if (angle_3_points <= M_PI/2. && angle_3_points >= 0) {
				est_dans_champs_de_vision = true;

			}
			if(est_dans_champs_de_vision) {
				//si chasseur dans le champs de vision, on regarde s'il y a un mur qui le cache
				bool mur_trouve_1 = false;
				bool mur_trouve_2 = false;
//prbl ici car les angles sont tjrs positifs donc on fait l'opération dans les deux sens
				float nv_angle = fmod(angle_3_points + radian, 2*M_PI); // un angle toujours en 0 et 2 pi c'est mieux
				float x_tempo = (float)gardien.x;
				float y_tempo = (float)gardien.y;
				float x_tempo_sauv = x_tempo;
				float y_tempo_sauv = y_tempo; 
				Labyrinthe* lab = (Labyrinthe*) _l; 
				// pour le y_tempo - 1, petite astuce inutile point de vue théorique.
				while(!(((int)x_tempo == chasseur_x) && ((int)y_tempo == chasseur_y || (int)y_tempo == chasseur_y - 1))) 
				{	
					if (lab->data((int)x_tempo,(int)y_tempo) == 1 && !((int)x_tempo == gardien.x &&
                                             (int)y_tempo == gardien.y) ) {
                                            	mur_trouve_1 = true;
						break;
					    }
					x_tempo = x_tempo - sin(nv_angle);
					y_tempo = y_tempo + cos(nv_angle);


				}
				
				if (!mur_trouve_1){
				
				inf_ch.trouve = true; inf_ch.angle = nv_angle; x_chasseur = chasseur_x;
				y_chasseur = chasseur_y; chasseur_just_seen = true;
				return inf_ch; }
				
				nv_angle = radian -  angle_3_points;
				//je met - 1 pour une meilleur précision
				while( !(((int)x_tempo_sauv == chasseur_x ) && ((int)y_tempo_sauv == chasseur_y || 
					(int)y_tempo_sauv == chasseur_y - 1 ))) 
				{	 
					if (lab->data((int)x_tempo_sauv,(int)y_tempo_sauv) == 1 && !((int)x_tempo_sauv == gardien.x &&
                                             (int)y_tempo_sauv == gardien.y) ) {
                                            	mur_trouve_2 = true;
						break;
					    }
					x_tempo_sauv = x_tempo_sauv - sin(nv_angle);
					y_tempo_sauv = y_tempo_sauv + cos(nv_angle);


				}

				if (!mur_trouve_2) { 
				
				inf_ch.trouve = true; inf_ch.angle = nv_angle; x_chasseur = chasseur_x;
				y_chasseur = chasseur_y; chasseur_just_seen = true;
				return inf_ch;}
				
			}

		}
       
	return inf_ch;

    }









bool Gardien::process_fireball (float dx, float dy)
{
	
 // calculer la distance entre le chasseur et le lieu de l'explosion.
	// on bouge que dans le vide!

float distance_gardien = ((((int)(_fb -> get_x () + dx) / Environnement::scale) - (((int)_l ->_guards[0]->_x) / Environnement::scale))*
			(((int)(_fb -> get_x () + dx) / Environnement::scale) - (((int)_l ->_guards[0]->_x) / Environnement::scale))) +
			((((int)(_fb -> get_y () + dy) / Environnement::scale) - (((int)_l ->_guards[0]->_y) / Environnement::scale)) *
			(((int)(_fb -> get_y () + dy) / Environnement::scale) - (((int)_l ->_guards[0]->_y) / Environnement::scale)));
distance_gardien = sqrt(distance_gardien);

	if (distance_gardien <1.3) //précision un peu houleuse par moment (très rare), je rajoute 0.3 pour la "securité" 

	{	message("Joueur touche, vie -1");
		//cout << ((Chasseur*)(_l ->_guards[0]))->get_point_de_vie() << endl;
		if ( ((Chasseur*)(_l ->_guards[0]))->get_point_de_vie() <= 0 ) {
		 partie_terminee(false); }
	     else { ((Chasseur*)(_l ->_guards[0]))->set_point_de_vie(((Chasseur*)(_l ->_guards[0]))->get_point_de_vie() -1) ;}		
		return false;

	}
	//il faut faire attention au fait que la boule de feu ne soit bloqué par le gardien lui-meme
	if (EMPTY == _l -> data ((int)((_fb -> get_x () + dx) / Environnement::scale),
							 (int)((_fb -> get_y () + dy) / Environnement::scale)) ||
          ( (int)((_fb -> get_x () + dx) / Environnement::scale) == (int)(_x/ Environnement::scale)  &&
	    (int)((_fb -> get_y () + dy) / Environnement::scale) == (int)(_y/ Environnement::scale)
	  ) )
	{		
		return true;
		
	}
	
	return false;

}

/*
 *	Tire sur un ennemi.
 */

void Gardien::fire (int angle_vertical)
{
	_fb -> init (/* position initiale de la boule */ _x, _y, 10.,
				 /* angles de visée */ angle_vertical, - _angle);
}

int Gardien::get_point_de_vie() {return point_de_vie;};
void Gardien::set_point_de_vie(int pt) { point_de_vie = pt; };

void Gardien::is_dead () {dead = true;}
bool Gardien::get_dead() {return dead;}
void Gardien::set_vue() {longueur_vision -= 5;};
